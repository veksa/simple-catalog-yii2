<?
namespace app\commands;

use Yii;
use yii\console\Controller;
use app\modules\catalog\models\Parser;

class ParserController extends Controller{
	public function actionIndex( $file = '' ){
		$filepath = Yii::getAlias( '@web' ) . '/upload/'.$file;

		if( $file && file_exists( $filepath ) ){
			$parser = new Parser( $filepath );
			if( $res = $parser->parse() ){
				$parser->fillBD( $res );
			}
		}else{
			echo 'file not found'."\n";
		}
	}
}
?>