<?
Yii::setAlias( '@web', dirname( __DIR__ ) . '/web' );

$params = array_merge( require( __DIR__ . '/params.php' ), require( __DIR__ . '/params-local.php' ) );

return [
	'id' => 'app-console',
	'basePath' => dirname( __DIR__ ),
	'bootstrap' => [
		'log'
	],
	'controllerNamespace' => 'app\commands',
	'components' => [
		'log' => [
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => [
						'error',
						'info'
					]
				]
			]
		]
	],
	'modules' => [
	],
	'params' => $params
];
