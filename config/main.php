<?
$params = array_merge( require( __DIR__ . '/params.php' ), require( __DIR__ . '/params-local.php' ) );

$config = [
	'id' => 'simple-catalog-yii2',
	'name' => 'simple-catalog-yii2',
	'basePath' => dirname( __DIR__ ),
	'bootstrap' => [ 'log' ],
	'defaultRoute' => 'main/default/index',
	'components' => [
		'request' => [
			// !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
			'cookieValidationKey' => 'O7Rk79g5ETH4Z-2GRaOrQ-PsHDE6RU8x'
		],
		'cache' => [
			'class' => 'yii\caching\FileCache',
		],
		'user' => [
			'identityClass' => 'app\modules\user\models\User',
			'enableAutoLogin' => true
		],
		'errorHandler' => [
			'errorAction' => 'main/default/error'
		],
		'log' => [
			'traceLevel' => YII_DEBUG ? 3 : 0,
			'targets' => [
				[
					'class' => 'yii\log\FileTarget',
					'levels' => [
						'error',
						'warning'
					],
				],
			],
		],
		'urlManager' => [
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'rules' => [
				'' => 'main/default/index',

				'catalog/id/<id:[\d]+>' => 'catalog/default/detail',
				'catalog/<id:[\d]+>' => 'catalog/default/category',
				'catalog' => 'catalog/default/category',

				'<_a:error>' => 'main/default/<_a>',
				'<_a:(login|logout)>' => 'user/default/<_a>'
			]
		]
	],
	'modules' => [
		'main' => [
			'class' => 'app\modules\main\Module'
		],
		'user' => [
			'class' => 'app\modules\user\Module'
		],
		'catalog' => [
			'class' => 'app\modules\catalog\Module'
		]
	],
	'params' => $params
];

if( YII_ENV_DEV ){
	// configuration adjustments for 'dev' environment
	$config['bootstrap'][] = 'debug';
	$config['modules']['debug'] = 'yii\debug\Module';

	$config['bootstrap'][] = 'gii';
	$config['modules']['gii'] = 'yii\gii\Module';
}

return $config;
