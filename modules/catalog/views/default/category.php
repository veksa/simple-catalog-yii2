<?
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = $model->name ? : 'Каталог';
$this->params['breadcrumbs'][] = [
	'label' => 'Каталог',
	'url' => [ '/catalog' ]
];

if( $category ){
	$this->params['breadcrumbs'][] = [
		'label' => $category->name,
		'url' => [ '/catalog/'.$category->id ]
	];
}
?>

<h1><?=$category ? $category->name : 'Каталог'?></h1>

<?if( $categoryProvider->getTotalCount() ){?>
	<?= GridView::widget( [
		'showHeader' => false,
		'dataProvider' => $categoryProvider,
		'columns' => [
			[
				'attribute' => 'name',
				'label' => 'Название',
				'format' => 'raw',
				'value' => function ( $model, $index, $dataColumn ){
					return Html::a( $model->name, '/catalog/'.$model->id );
				}
			]
		],
		'layout' => "{items}\n{pager}"
	] ) ?>
<?}?>

<?= GridView::widget( [
	'dataProvider' => $itemProvider,
	'filterModel' => $itemSearch,
	'columns' => [
		[
			'attribute' => 'name',
			'label' => 'Название',
			'value' => function ( $model, $index, $dataColumn ){
				return $model->name;
			}
		],
		[
			'attribute' => 'article',
			'value' => function ( $model, $index, $dataColumn ){
				return $model->article;
			}
		],
		[
			'attribute' => 'brand_name',
			'value' => function ( $model, $index, $dataColumn ){
				return $model->brand->name;
			}
		],
		[
			'attribute' => 'model',
			'value' => function ( $model, $index, $dataColumn ){
				return $model->model;
			}
		],
		[
			'attribute' => 'orientation',
			'value' => function ( $model, $index, $dataColumn ){
				return $model->orientation;
			}
		],
		[
			'attribute' => 'quantity',
			'value' => function ( $model, $index, $dataColumn ){
				return $model->quantity;
			}
		],
		[
			'attribute' => 'colors',
			'value' => function ( $model, $index, $dataColumn ){
				$colors = [ ];
				if( $model->colors ){
					foreach( $model->colors as $color ){
						$colors[] = $color->name;
					}
				}

				return implode( ', ', $colors );
			}
		],
		[
			'attribute' => 'sizes',
			'value' => function ( $model, $index, $dataColumn ){
				$sizes = [ ];
				if( $model->sizes ){
					foreach( $model->sizes as $size ){
						$sizes[] = $size->name;
					}
				}

				return implode( ', ', $sizes );
			}
		]
	]
] ) ?>