<?
namespace app\modules\catalog;

class Module extends \yii\base\Module{
	public $controllerNamespace = 'app\modules\catalog\controllers';
	public $commandsNamespace = 'app\modules\catalog\commands';

	public function init(){
		parent::init();
	}
}
