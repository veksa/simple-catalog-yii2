<?
namespace app\modules\catalog\models;

use Yii;

class Brand extends \yii\db\ActiveRecord{
	public static function tableName(){
		return 'brand';
	}

	public function rules(){
		return [
			[
				[ 'name' ],
				'required'
			],
			[
				[ 'name' ],
				'string',
				'max' => 255
			]
		];
	}

	public function attributeLabels(){
		return [
			'id' => 'ID',
			'name' => 'Название'
		];
	}

	public function getItems(){
		return $this->hasMany( Item::className(), [ 'brand_id' => 'id' ] );
	}
}