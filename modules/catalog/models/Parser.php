<?
namespace app\modules\catalog\models;

use Yii;
use yii\console\Controller;

class Parser{
	private $output = [ ];

	// dir -> subdir -> makros
	public static $catalog = [
		'Одежда' => [
			'Трусы' => [
				'Трусы вратарские',
				'Трусы вратаря',
				'Судейские трусы',
				'Трусы судейские',
				'Трусы хоккейные',
				'Трусы игрока',
				'Трусы'
			],
			'Брюки' => [
				'Брюки с раковиной',
				'Спортивное белье вратаря (брюки)',
				'Брюки вратаря',
				'Брюки'
			],
			'Шорты' => [
				'Шорты вратарские',
				'Шорты-бандаж',
				'Шорты с раковиной'
			],
			'Нижнее белье' => [
				'Белье хоккейное нат.(комбин)',
				'Нижнее белье',
				'Белье нижнее',
				'Белье'
			],
			'Гамаши' => [
				'Гамаши хоккейные',
				'Гамаши'
			],
			'Джемпера' => [
				'Форма хоккейная (джемпер вратаря)',
				'Форма хоккейная (джемпер)'
			],
			'Толтовки' => [
				'Толстовка с капюшоном'
			],
			'Кофты' => [
				'Кофта NITRO с капюшоном',
				'Кофта с капюшоном'
			],
			'Свитера' => [
				'Игровой свитер'
			],
			'Куртки' => [
				'Куртка утепленная',
				'Флисовая куртка',
				'Ветронепроницаемая куртка',
				'Куртка на молнии',
				'Куртка с подкладкой',
				'Куртка спортивная',
				'Куртка'
			],
			'Рубашки' => [
				'Спортивное белье вратаря (Рубашка)',
				'Вратарская тренировочная рубашка',
				'Тренировочная рубашка вратаря',
				'Тренировочная рубашка',
				'Рубашка'
			],
			'Майки' => [
				'Тренировочная майка',
				'Майка дл/рук',
				'Майка'
			],
			'Поло' => [
				'Поло'
			],
			'Футболки' => [
				'Футболка'
			],
			'Костюмы' => [
				'Тренеровочный костюм',
				'Костюм тренеровочный',
				'Костюм утепленный',
				'Костюм ветрозащитный',
				'Костюм спортивный',
				'Костюм'
			],
			'Сандали' => [
				'Сандали'
			],
			'Шнурки' => [
				'Шнурки хоккейные с пропиткой',
				'Шнурки для коньков'
			],
			'Подтяжки' => [
				'Подтяжки с клипсами',
				'Подтяжки с петлями',
				'Подтяжки'
			]
		],
		'Защита' => [
			'Раковины' => [
				'Раковина с поясом для гамаш',
				'Раковина вратаря женская',
				'Раковина вратаря',
				'Раковина с подвязками',
				'Женская раковина',
				'Раковина с подтяжками (3 in 1)',
				'Раковина'
			],
			'Защита шеи' => [
				'Защита шеи вратаря',
				'Защита шеи'
			],
			'Защита запястья' => [
				'Защита запястья'
			],
			'Защита локтя' => [
				'Защита локтя'
			],
			'Защита голени' => [
				'Защита голени'
			],
			'Защита горла' => [
				'Защита горла вратаря',
				'Защита горла игрока',
				'Защита горла'
			],
			'Защита плеч' => [
				'Защита плеч'
			],
			'Защита лица' => [
				'Защита лица'
			],
			'Паховая защита' => [
				'Паховая защита вратаря'
			],
			'Налокотники' => [
				'Налокотник'
			],
			'Перчатки' => [
				'Перчатки игрока',
				'Перчатки'
			],
			'Нагрудники' => [
				'Нагрудник вратаря',
				'Нагрудник'
			],
			'Шлемы' => [
				'Шлем игрока с маской',
				'Шлем вратаря',
				'Шлем игрока',
				'Шлем с маской',
				'Шлем'
			],
			'Маски, визоры' => [
				'Маска вратаря',
				'Визор для шлема',
				'Визор-маска',
				'Маска-визор',
				'Маска 480',
				'Маска',
				'Визор'
			],
			'Блокеры' => [
				'Блокер вратарский'
			],
			'Щитки' => [
				'Щитки вратарские',
				'Щитки вратаря'
			],
			'Решетки' => [
				'Защитная решетка на шлем вратаря'
			]
		],
		'Аксессуары' => [
			'Пояса' => [
				'Пояс для гамаш'
			],
			'Подтяжки' => [
				'Подтяжки с клипсами',
				'Подтяжки с петлями',
				'Подтяжки'
			],
			'Ремни' => [
				'Сменный ремень для трусов',
				'Ремень для трусов'
			],
			'Ловушки' => [
				'Ловушка вратарская',
				'Ловушка вратаря'
			],
			'Блины' => [
				'Блин вратарский',
				'Блин вратаря'
			],
			'Сумки' => [
				'Сумка вратаря с колесами',
				'Сумка вратаря для щитков',
				'Сумка вратарская с колесами',
				'Сумка вратарская',
				'Сумка вратаря',
				'Сумка спортивная',
				'Сумка для коньков',
				'Сумка для 3-х клюшек',
				'Сумка с колесами',
				'Сумка для 36 клюшек на колесах',
				'Сумка тренера',
				'Сумка хоккейная с колесами-органайзер',
				'Сумка для шлема',
				'Сумка'
			],
			'Чехлы' => [
				'Чехлы для коньков',
				'Чехол для трусов'
			],
			'Ругуляторы' => [
				'Регулятор жесткости конька'
			],
			'Шайбы' => [
				'Шайба хоккейная тренировочная облегченная'
			]
		],
		'Запасные части' => [
			'Трубки' => [
				'Композитная трубка'
			],
			'Рукоятки' => [
				'Рукоятка композитная',
				'Композитная рукоятка'
			],
			'Лопасти' => [
				'Лопасть композитная',
				'Сменная лопасть'
			],
			'Крюки' => [
				'Композитный крюк'
			],
			'Сетки' => [
				'Сетка для хоккейных ворот тренировочная (нить 2,2 мм) с гасителями'
			],
			'Гасители' => [
				'Гаситель для профессиональной хоккейной сетки (нить 3,1 мм)'
			],
			'Стаканы' => [
				'Стакан без лезвия',
				'Стакан в сборе'
			],
			'Лезвия' => [
				'Лезвие'
			],
			'Корректоры' => [
				'Алмазный корректор заточного диска'
			],
			'Крепежи' => [
				'Крепеж для лезвий'
			],
			'Винты' => [
				'Запасные винты к лезвиям'
			],
			'Камни для заточки' => [
				'Камень для заточки коньков',
				'Камень для заточки'
			]
		],
		'Клюшки' => [
			'Клюшки вратарские' => [
				'Клюшка вратарская',
				'Клюшка вратаря',
				'Клюшка вратарская',
				'Вратарская клюшка'
			],
			'Клюшки игрока' => [
				'Клюшка дер. с композитным крюком',
				'Клюшка композитная',
				'Клюшка цельнокомпозитная',
				'Композитная клюшка',
				'Клюшка игрока',
				'Клюшка деревянная',
				'Клюшка для ринк-бенди',
				'Клюшки',
				'Клюшка'
			]
		],
		'Коньки' => [
			'Коньки вратарские' => [
				'Коньки вратарские',
				'Коньки вратаря'
			],
			'Коньки игрока' => [
				'Коньки детс. раздвижные',
				'Коньки хоккейные',
				'Коньки игрока',
				'Коньки'
			]
		]
	];

	// brand - makros
	public static $brands = [
		'OAKLEY' => [
			'OAKLEY'
		],
		'Pallas' => [
			'Pallas 7220',
			'Pallas 265',
			'Pallas 263',
			'Pallas 255',
			'Pallas 253',
			'Pallas 247',
			'Pallas 246',
			'Pallas 243',
			'Pallas 2205',
			'Pallas 224',
			'Pallas 223',
			'Pallas 222',
			'Pallas 208',
			'Pallas 206',
			'Pallas 205',
			'Pallas 204',
			'Pallas 203',
			'Pallas 202',
			'Pallas 154',
			'Pallas 103',
			'Pallas 105',
			'Pallas 106',
			'Pallas 108',
			'Pallas 131',
			'Pallas 165',
			'Pallas 163',
			'Pallas 165',
			'Pallas'
		],
		'CCM' => [
			'CCM 0390',
			'CCM 0630',
			'CCM 0390',
			'CCM'
		],
		'Reebok' => [
			'Reebok'
		],
		'BAUER' => [
			'BAUER'
		],
		'Tackla' => [
			'Tackla'
		],
		'Torspo' => [
			'Torspo 610',
			'Torspo Surge 221',
			'Torspo GB',
			'Torspo GP',
			'Torspo AB',
			'Torspo',
		],
		'NBH' => [
			'NBH'
		],
		'Easton' => [
			'Easton'
		],
		'Montreal' => [
			'Montreal'
		],
		'Capitals' => [
			'Capitals'
		],
		'Акбарс' => [
			'Акбарс'
		],
		'Канадиенс' => [
			'Канадиенс'
		],
		'Mad Guy' => [
			'Mad Guy'
		],
		'MISSION' => [
			'MISSION'
		],
		'Jofa' => [
			'Jofa 3030',
			'Jofa 3075',
			'Jofa'
		],
		'TPS' => [
			'TPS'
		],
		'RBK' => [
			'RBK 3040',
			'RBK 3065',
			'RBK'
		],
		'SWS' => [
			'SWS'
		],
		'Sher-Wood' => [
			'Sher-Wood'
		],
		'TAC' => [
			'TAC 377',
			'TAC'
		],
		'Kosa' => [
			'Kosa'
		],
		'FORTUNA' => [
			'FORTUNA'
		],
		'Salming' => [
			'Salming'
		],
		'KOHO' => [
			'KOHO 3385',
			'KOHO 4490',
			'KOHO 2285',
			'KOHO 225',
			'KOHO'
		],
		'GRAF' => [
			'Graf 101',
			'Graf 305',
			'Graf 405',
			'Graf 451',
			'Graf 301',
			'Graf 505',
			'Graf 702',
			'Graf 707',
			'Graf 501',
			'Graf 503',
			'Graf 505',
			'Graf 705',
			'Graf 707',
			'Graf 709',
			'Graf 735',
			'GRAF'
		],
		'OXDOG' => [
			'OXDOG'
		],
		'Wall' => [
			'Wall'
		],
		'SSM' => [
			'SSM'
		],
		'Gufex' => [
			'Gufex'
		],
		'Vegum' => [
			'Vegum'
		],
		'Viking' => [
			'Viking 603',
			'Viking'
		],
		'GRIT' => [
			'GRIT'
		]
	];

	// color -> makros
	public static $colors = [
		'темно-сине-бело-красный' => [
			'т-син/бел/крас',
			'т-син/крас/бел',
			'бел/т.син/крас'
		],
		'бело-красно-синий' => [
			'бел/крас/син',
			'бел/син/крас'
		],
		'бело-сине-серый' => [
			'бел/син/сер',
			'бел/сер/син'
		],
		'бело-черно-серый' => [
			'бел/черн/сер',
			'черн/бел/сер'
		],
		'бело-зелено-черный' => [
			'зелен/бел/чер'
		],
		'бело-красно-черный' => [
			'черно/крас/бел',
			'бел/крас/чер',
			'крас/черн/бел'
		],
		'бело-черно-желтый' => [
			'желт/черн/бел'
		],
		'сине-красно-золотой' => [
			'син/крас/золот'
		],
		'бело-черно-золотой' => [
			'бел/черн/золот'
		],
		'черно-желтый' => [
			'черно/желтые',
			'желт/черн',
			'желто/черные',
			'черн/желт.'
		],
		'бело-синий' => [
			'бел/син',
			'бело/синие',
			'сине/белые'
		],
		'бело-красный' => [
			'бел/крас',
			'красно/белые'
		],
		'черно-белый' => [
			'черно/бел',
			'черн/белые',
			'черн/бел',
			'бел/черн',
			'черн/бел'
		],
		'темно-сине-белый' => [
			'т-син/бел'
		],
		'темно-сине-черный' => [
			'т-син/черн'
		],
		'зелено-серый' => [
			'зел/сер'
		],
		'сине-красный' => [
			'син/крас'
		],
		'черно-красный' => [
			'черн/красн',
			'черн/крас'
		],
		'черно-серый' => [
			'черн/сер'
		],
		'черно-золотой' => [
			'черн/зол'
		],
		'черно-оранжевый' => [
			'черн/оранж'
		],
		'черный' => [
			'черный',
			'черная',
			'черные',
			'черн',
			'чер.'
		],
		'серый' => [
			'серый',
			'серая'
		],
		'темно-синий' => [
			'т.синий',
			'темно-синие',
			'т.синяя',
			'т-синяя',
			'т-синие',
			'т.-синие'
		],
		'желтый' => [
			'желтый',
			'желтая',
			'желт',
		],
		'красный' => [
			'красный',
			'крас.'
		],
		'синий' => [
			'синий',
			'синие',
			'синяя',
			'син.'
		],
		'белый' => [
			'белый',
			'белая',
			'бел.'
		],
		'оранжевый' => [
			'оранж'
		],
		'золотой' => [
			'золотой',
			'золото'
		],
		'хром' => [
			'хром'
		]
	];

	public static $clothes_sizes = [
		0 => [
			'XXXS' => [
				'XXXS',
				'0=120',
				'120'
			]
		],
		1 => [
			'XXS' => [
				'1=140',
				'140',
				'XXS'
			]
		],
		2 => [
			'XXL' => [
				'XXL'
			]
		],
		3 => [
			'XL' => [
				'7=XL',
				'XL'
			]
		],
		4 => [
			'XS' => [
				'XS=150',
				'XS'
			]
		],
		5 => [
			'L' => [
				'L'
			]
		],
		6 => [
			'SR' => [
				'взрослые',
				'Senior',
				'SR'
			]
		],
		7 => [
			'MID' => [
				'MID'
			]
		],
		8 => [
			'S' => [
				'Boy(S)',
				'S 100',
				'S=160',
				'4=S',
				'160',
				'S'
			]
		],
		9 => [
			'M' => [
				'M',
				'170'
			]
		],
		10 => [
			'JR' => [
				'юниор',
				'JR'
			]
		],
		11 => [
			'PW' => [
				'PW'
			]
		],
		12 => [
			'Boy' => [
				'Boy'
			]
		],
		13 => [
			'YTH' => [
				'детские',
				'детс.',
				'YTH'
			]
		],
		14  => [
			'244 см' => [
				'244 см'
			]
		],
		15 => [
			'274 см' => [
				'274 см'
			]
		],
		16 => [
			'112' => [
				'112'
			]
		],
		17 => [
			'102' => [
				'102'
			]
		],
		18 => [
			'54' => [
				'54'
			]
		],
		19 => [
			'52' => [
				'52'
			]
		],
		20 => [
			'50' => [
				'50'
			]
		],
		21 => [
			'48' => [
				'48'
			]
		],
		22 => [
			'46' => [
				'46'
			]
		],
		23 => [
			'45' => [
				'45'
			]
		],
		24 => [
			'44' => [
				'44'
			]
		],
		25 => [
			'42' => [
				'42'
			]
		],
		26 => [
			'40' => [
				'40'
			]
		],
		27 => [
			'38' => [
				'38'
			]
		],
		28 => [
			'33' => [
				'33'
			]
		]
	];

	public static $stick_sizes = [
		0 => [
			'22.5' => [
				'22.5'
			]
		],
		1 => [
			'11,5' => [
				'11,5',
				'11.5'
			]
		],
		2 => [
			'10.5' => [
				'10,5(D)',
				'10,5',
				'10.5'
			]
		],
		3 => [
			'9.5' => [
				'9.5  W',
				'9.5'
			]
		],
		4 => [
			'8.5' => [
				'8.5 (D)',
				'8.5  W',
				'8.5'
			]
		],
		5 => [
			'7.5' => [
				'7.5 (D)',
				'7.5'
			]
		],
		6 => [
			'6.5' => [
				'6.5 (D)',
				'6.5',
				'6,5'
			]
		],
		7 => [
			'4.5' => [
				'4,5 (ЕЕ)',
				'4,5 (D)',
				'4,5',
				'4.5'
			]
		],
		8 => [
			'3.5' => [
				'3,5 (ЕЕ)',
				'3.5'
			]
		],
		9 => [
			'2.5' => [
				'2,5 (D)',
				'2.5'
			]
		],
		10 => [
			'1.5' => [
				'1,5 (2E)',
				'1.5 (ЕЕ)',
				'1.5'
			]
		],
		11 => [
			'112' => [
				'112'
			]
		],
		12 => [
			'102' => [
				'102'
			]
		],
		13 => [
			'95' => [
				'95'
			]
		],
		14 => [
			'87' => [
				'87'
			]
		],
		15 => [
			'77' => [
				'77'
			]
		],
		16 => [
			'54' => [
				'54'
			]
		],
		17 => [
			'52' => [
				'52'
			]
		],
		18 => [
			'50' => [
				'50'
			]
		],
		19 => [
			'YTH' => [
				'YTH'
			]
		],
		20 => [
			'PW' => [
				'PW'
			]
		],
		21 => [
			'JR' => [
				'JR'
			]
		],
		22 => [
			'SR' => [
				'SR'
			]
		]
	];

	public static $skates_sizes = [
		0 => [
			'47' => [
				'47'
			]
		],
		1 => [
			'45' => [
				'45'
			]
		],
		2 => [
			'44' => [
				'44'
			]
		],
		3 => [
			'43' => [
				'43'
			]
		],
		4 => [
			'42' => [
				'42'
			]
		],
		5 => [
			'40' => [
				'40'
			]
		],
		6 => [
			'39' => [
				'39'
			]
		],
		7 => [
			'38' => [
				'38'
			]
		],
		8 => [
			'37' => [
				'37'
			]
		],
		9 => [
			'36' => [
				'36'
			]
		],
		10 => [
			'35' => [
				'35'
			]
		],
		11 => [
			'34' => [
				'34'
			]
		],
		12 => [
			'33' => [
				'33'
			]
		],
		13 => [
			'32' => [
				'32'
			]
		],
		14 => [
			'30' => [
				'30'
			]
		],
		15 => [
			'28' => [
				'28'
			]
		],
		16 => [
			'27' => [
				'27'
			]
		],
		17 => [
			'20' => [
				'20'
			]
		],
		18 => [
			'15' => [
				'15'
			]
		],
		19 => [
			'13' => [
				'13'
			]
		],
		20 => [
			'12' => [
				'12'
			]
		],
		21 => [
			'11' => [
				'11(D)',
				'11'
			]
		],
		22 => [
			'10' => [
				'10 (D)',
				'10 D',
				'10'
			]
		],
		23 => [
			'8' => [
				'8 (D)',
				'8  W',
				'8'
			]
		],
		24 => [
			'7' => [
				'7 (EE)',
				'7 (D)',
				'7 (2E)',
				'7'
			]
		],
		25 => [
			'6' => [
				'6 (2E)',
				'6 (ЕЕ)',
				'6(D)',
				'6'
			]
		],
		26 => [
			'5' => [
				'5 (D)',
				'5'
			]
		],
		27 => [
			'4' => [
				'4 (ЕЕ)',
				'4'
			]
		],
		28 => [
			'3' => [
				'3 (ЕЕ)',
				'3 (D)',
				'3'
			]
		],
		29 => [
			'2' => [
				'2 (D)',
				'2'
			]
		],
		30 => [
			'1' => [
				'1 (EE)',
				'1 (D)',
				'1'
			]
		],
		31 => [
			'YTH' => [
				'YTH'
			]
		],
		32 => [
			'PW' => [
				'PW'
			]
		],
		33 => [
			'JR' => [
				'JR'
			]
		],
		34 => [
			'SR' => [
				'SR'
			]
		]
	];

	private $file;

	public function __construct( $file ){
		$this->file = fopen( $file, 'r' );
		if( $this->file === false ){
			echo 'error open file: ' . $file;
		}
	}

	private function clearVars(){
		$this->output = [
			'dir' => '',
			'brand' => '',
			'model' => '',
			'color' => '',
			'size' => '',
			'orientation' => '',
			'article' => ''
		];
	}

	public function explodeBrand( $str ){
		foreach( self::$brands as $name => $brand ){
			foreach( $brand as $name2 ){
				if( preg_match( '/' . $name2 . '/i', $str, $match ) ){
					$this->output['brand'] = $name;

					return str_replace( $match[0], '', $str );
				}
			}
		}

		return $str;
	}

	public function explodeColor( $str ){
		foreach( self::$colors as $name => $colors ){
			foreach( $colors as $color ){
				if( preg_match( '/' . preg_quote( $color, '/' ) . '/i', $str, $match ) ){
					$this->output['color'] = $name;

					return str_replace( $match[0], '', $str );
				}
			}
		}

		return $str;
	}

	public function explodeSize( $str ){
		$res = $str;

		foreach( self::$clothes_sizes as $inx => $ar ){
			foreach( $ar as $name => $sizes ){
				foreach( $sizes as $name2 ){
					if( preg_match( '/\b' . preg_quote( $name2, '/' ) . '$/i', $str, $match ) ){
						$this->output['size'] = $name;

						$res = preg_replace( '/\b' . preg_quote( $match[0], '/' ) . '$/i', '', $res );

						break 2;
					}

					if( preg_match( '/\b' . preg_quote( $name2, '/' ) . ',/i', $str, $match ) ){
						$this->output['size'] = $name;

						$res = str_replace( $match[0], '', $res );

						break 2;
					}

					if( preg_match( '/\b' . preg_quote( $name2, '/' ) . '\b/i', $str, $match ) ){
						$this->output['size'] = $name;

						$res = preg_replace( '/\b' . preg_quote( $name2, '/' ) . '\b/i', '', $str );

						break 2;
					}
				}
			}
		}

		$res = preg_replace( '/' . 'YTH' . '/i', '', $res );
		$res = preg_replace( '/' . 'SR' . '/i', '', $res );
		$res = preg_replace( '/' . 'MID' . '/i', '', $res );
		$res = preg_replace( '/' . 'JR' . '/i', '', $res );
		$res = preg_replace( '/' . 'SR' . '/i', '', $res );

		return $res;
	}

	public function explodeOrientation( $str ){
		$ar = [
			'L',
			'R'
		];

		foreach( $ar as $k ){
			if( preg_match( '/\b' . $k . '\b/i', $str ) ){
				$this->output['orientation'] = $k;

				return preg_replace( '/\s' . $k . '$/i', '', $str );
			}
		}

		return $str;
	}

	public function explodeStickSize( $str ){
		$res = $str;

		foreach( self::$stick_sizes as $inx => $ar ){
			foreach( $ar as $name => $sizes ){
				foreach( $sizes as $name2 ){
					if( preg_match( '/' . preg_quote( $name2, '/' ) . '$/i', $res, $match ) ){
						$this->output['size'] = $name;

						$res = preg_replace( '/' . preg_quote( $match[0], '/' ) . '$/i', '', $res );

						break 2;
					}

					if( preg_match( '/\-' . preg_quote( $name2, '/' ) . '\s/i', $res, $match ) ){
						$this->output['size'] = $name;

						$res = preg_replace( '/\-' . preg_quote( $match[0], '\s/' ) . '/i', '', $res );

						break 2;
					}
				}
			}
		}

		$res = preg_replace( '/' . 'YTH' . '/i', '', $res );
		$res = preg_replace( '/' . 'SR' . '/i', '', $res );
		$res = preg_replace( '/' . 'MID' . '/i', '', $res );
		$res = preg_replace( '/' . 'JR' . '/i', '', $res );
		$res = preg_replace( '/' . 'SR' . '/i', '', $res );

		return $res;
	}

	public function explodeSkatesSize( $str ){
		$res = $str;

		foreach( self::$skates_sizes as $inx => $ar ){
			foreach( $ar as $name => $sizes ){
				foreach( $sizes as $name2 ){
					if( preg_match( '/' . preg_quote( $name2, '/' ) . '$/i', $res, $match ) ){
						$this->output['size'] = $name;

						$res = preg_replace( '/' . preg_quote( $match[0], '/' ) . '$/i', '', $res );

						break 2;
					}
				}
			}
		}

		$res = preg_replace( '/' . 'YTH' . '/i', '', $res );
		$res = preg_replace( '/' . 'SR' . '/i', '', $res );
		$res = preg_replace( '/' . 'MID' . '/i', '', $res );
		$res = preg_replace( '/' . 'JR' . '/i', '', $res );
		$res = preg_replace( '/' . 'SR' . '/i', '', $res );

		return $res;
	}

	public function explodeArticle( $str ){
		if( preg_match( '/\w?[\d]{5,}/i', $str, $match ) ){
			$this->output['article'] = $match[0];

			return str_replace( $match[0], '', $str );
		}

		return $str;
	}

	public function explodeName( $str ){
		foreach( self::$catalog as $name => $dir ){
			foreach( $dir as $subname => $subdir ){
				foreach( $subdir as $makros ){
					if( preg_match( '/' . preg_quote( $makros, '/' ) . '/i', $str, $match ) ){
						$this->output['dir'] = $subname;
						$this->output['name'] = $match[0];

						return str_replace( $match[0], '', $str );
					}
				}
			}
		}

		return $str;
	}

	public function parse(){
		if( !$this->file ){
			return Controller::EXIT_CODE_ERROR;
		}

		$res = [];

		$cnt = 0;

		while( !feof( $this->file ) ){
			$this->clearVars();
			$str = fgets( $this->file );

			if( !$str ){
				continue;
			}

			Yii::info( $str );

			$str = str_replace( [
				'"',
			], '', $str );

			$str = str_replace( [
				'  ',
				'(100% хлопок)',
				'(35% хл+65% ПЭ)'
			], ' ', $str );

			$str = $this->explodeBrand( $str );
			$str = $this->explodeColor( $str );
			$str = $this->explodeArticle( $str );

			$str = str_replace( [
				'()',
				'( )'
			], '', $str );

			while( strpos( $str, '  ' ) !== false ){
				$str = str_replace( '  ', ' ', $str );
			}

			$str = $this->explodeName( $str );

			if( $this->output['dir'] === 'Клюшки игрока' || $this->output['dir'] === 'Клюшки вратарские' ){
				$str = $this->explodeOrientation( $str );
				$str = $this->explodeStickSize( $str );
			}elseif( $this->output['dir'] === 'Коньки вратарские' || $this->output['dir'] === 'Коньки игрока' ){
				$str = $this->explodeSkatesSize( $str );
			}else{
				$str = trim( $str );
				$str = $this->explodeSize( $str );
			}

			$str = preg_replace( '/\(.*\)/i', '', $str );

			$str = str_replace( [
				'- ',
				' -',
				','
			], '', $str );

			while( strpos( $str, '  ' ) !== false ){
				$str = str_replace( '  ', ' ', $str );
			}

			$str = trim( $str );

			$this->output['model'] = $str;

			$this->output['fullname'] = implode( ' ', array_filter( [
				$this->output['name'],
				$this->output['brand'],
				$this->output['model'],
				$this->output['article'],
				$this->output['size']
			], function ( $item ){
				return (bool)( $item );
			} ) );

			Yii::info( $this->output );

			$key = md5( $this->output['fullname'] );
			if( !array_key_exists( $key, $res ) ){
				$this->output['quantity'] = 1;

				$this->output['color'] = [ $this->output['color'] ];

				$res[$key] = $this->output;
			}else{
				$this->output['quantity'] = $res[$key]['quantity']++;

				if( $this->output['color'] && !in_array( $this->output['color'], $res[$key]['color'], true ) ){
					$res[$key]['color'][] = $this->output['color'];
				}

				$this->output['color'] = $res[$key]['color'];
			}

			$cnt++;
		}

		echo $cnt."\n";

		return $res;
	}

	public function fillBD( $res ){
		$category = [];
		$data = Category::find()->where( [ 'not', [ 'parent_id' => NULL ] ] )->all();
		foreach( $data as $model ){
			$category[$model->name] = $model->id;
		}

		$brands = [];
		$data = Brand::find()->all();
		foreach( $data as $model ){
			$brands[$model->name] = $model->id;
		}

		$colors = [];
		$data = Color::find()->all();
		foreach( $data as $model ){
			$colors[$model->name] = $model->id;
		}

		$sizes = [];
		$data = Size::find()->all();
		foreach( $data as $model ){
			$sizes[$model->name] = $model->id;
		}

		foreach( $res as $item ){
			Yii::info( $item );

			$model = new Item;
			if( $item['name'] ){
				$model->name = $item['name'];
			}else{
				$model->name = 'Имя не указано';
			}

			$model->article = $item['article'];

			if( array_key_exists( $item['dir'], $category ) ){
				$model->parent_id = $category[$item['dir']];
			}else{
				$model->parent_id = NULL;
			}

			if( array_key_exists( $item['brand'], $brands ) ){
				$model->brand_id = $brands[$item['brand']];
			}else{
				$model->brand_id = NULL;
			}

			$model->model = $item['model'];
			$model->color = [];
			foreach( $item['color'] as $color ){
				if( $id = array_search( $color, $colors, true ) ){
					$model->color[] = $id;
				}
			}

			if( array_key_exists( $item['size'], $sizes ) ){
				$model->size = [ $sizes[$item['size']] ];
			}else{
				$model->size = [];
			}

			$model->orientation = $item['orientation'];
			$model->quantity = (int)$item['quantity'];

			$model->save();
		}

		echo 'done import'."\n";
	}
}