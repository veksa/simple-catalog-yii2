<?
namespace app\modules\catalog\models;

use Yii;

class Size extends \yii\db\ActiveRecord{
	public static function tableName(){
		return 'size';
	}

	public function rules(){
		return [
			[
				[ 'name' ],
				'required'
			],
			[
				[ 'name' ],
				'string',
				'max' => 255
			]
		];
	}

	public function attributeLabels(){
		return [
			'id' => 'ID',
			'name' => 'Название',
		];
	}

	public function getItems(){
		return $this->hasMany( Item::className(), [ 'id' => 'item_id' ] )
			->viaTable( 'item_size', [
				'size_id' => 'id'
			] );
	}
}