<?
namespace app\modules\catalog\models;

use Yii;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

class Category extends ActiveRecord{
	public static function tableName(){
		return 'category';
	}

	public function rules(){
		return [
			[
				[ 'name' ],
				'required'
			],
			[
				[ 'parent_id' ],
				'integer'
			],
			[
				[ 'name' ],
				'string',
				'max' => 255
			]
		];
	}

	public function attributeLabels(){
		return [
			'id' => 'ID',
			'name' => 'Name',
			'parent_id' => 'Parent ID'
		];
	}

	public function getParent(){
		return $this->hasOne( Category::className(), [ 'id' => 'parent_id' ] );
	}

	public function getCategories(){
		return $this->hasMany( Category::className(), [ 'parent_id' => 'id' ] );
	}

	public function getItems(){
		return $this->hasMany( Item::className(), [ 'parent_id' => 'id' ] );
	}

	public function getCategoryList( $parent_id ){
		return new ActiveDataProvider( [
			'query' => Category::find()
				->where( [
					'parent_id' => $parent_id
				] )
				->orderBy( 'id ASC' )
		] );
	}

	public static function getChildrens( $id ){
		// TODO: mv to left_margin and right_margin query
		$tmp = [ ];

		$categories = Category::find()
			->where( [
				'parent_id' => $id
			] )
			->all();

		if( $categories ){
			foreach( $categories as $item ){
				$key = $item->getPrimaryKey();

				$tmp[] = $key;
				$subtmp = self::getChildrens( $key );

				$tmp = array_merge( $tmp, $subtmp );
			}
		}

		return $tmp;
	}
}