<?
namespace app\modules\catalog\models;

use Yii;
use yii\db\ActiveRecord;
use voskobovich\behaviors\ManyToManyBehavior;

class Item extends ActiveRecord{
	public static function tableName(){
		return 'item';
	}

	public function rules(){
		return [
			[
				[
					'parent_id',
					'quantity'
				],
				'integer'
			],
			[
				[
					'name',
					'article',
					'orientation',
					'model'
				],
				'string',
				'max' => 255
			],
			[
				[
					'article',
					'brand_name'
				],
				'safe'
			]
		];
	}

	public function attributeLabels(){
		return [
			'id' => 'ID',
			'name' => 'Название',
			'parent_id' => 'Раздел',
			'article' => 'Артикул',
			'brand_name' => 'Бренд',
			'model' => 'Модель',
			'orientation' => 'Ориентация',
			'colors' => 'Цвета',
			'sizes' => 'Размер',
			'quantity' => 'Количество'
		];
	}

	public function attributes(){
		return array_merge( parent::attributes(), [ 'brand_name' ] );
	}

	public function getAttributes( $names = NULL, $except = [ ] ){
		$attributes = parent::getAttributes( $names = NULL, $except = [ ] );

		return array_replace( $attributes, [
			'colors' => $this->colors,
			'sizes' => $this->sizes
		] );
	}

	public function behaviors(){
		return [
			[
				'class' => ManyToManyBehavior::className(),
				'relations' => [
					'color' => 'colors'
				]
			],
			[
				'class' => ManyToManyBehavior::className(),
				'relations' => [
					'size' => 'sizes'
				]
			]
		];
	}

	public function getParent(){
		return $this->hasOne( Category::className(), [ 'id' => 'parent_id' ] );
	}

	public function getBrand(){
		return $this->hasOne( Brand::className(), [ 'id' => 'brand_id' ] );
	}

	public function getColors(){
		return $this->hasMany( Color::className(), [ 'id' => 'color_id' ] )
			->viaTable( 'item_color', [
				'item_id' => 'id'
			] );
	}

	public function getSizes(){
		return $this->hasMany( Size::className(), [ 'id' => 'size_id' ] )
			->viaTable( 'item_size', [
				'item_id' => 'id'
			] );
	}
}