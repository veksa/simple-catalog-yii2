<?
namespace app\modules\catalog\models;

use yii\data\ActiveDataProvider;

class ItemSearch extends Item{
	public function search( $parent_id, $params ){
		$query = Item::find()->where( [
			'parent_id' => array_merge( [ $parent_id ], Category::getChildrens( $parent_id ) )
		] );

		$query->joinWith( [
			'brand'
		] );

		$dataProvider = new ActiveDataProvider( [
			'query' => $query,
			'pagination' => [
				'pageSize' => 10
			]
		] );

		$dataProvider->sort->attributes['brand_name'] = [
			'asc' => [ 'brand.name' => SORT_ASC ],
			'desc' => [ 'brand.name' => SORT_DESC ]
		];

		if( !( $this->load( $params ) && $this->validate() ) ){
			return $dataProvider;
		}

		$query->andFilterWhere( [
			'like',
			'item.name',
			$this->name
		] )
			->andFilterWhere( [
				'like',
				'article',
				$this->article
			] )
			->andFilterWhere( [
				'like',
				'brand.name',
				$this->brand_name
			] )
			->andFilterWhere( [
				'like',
				'model',
				$this->model
			] )
			->andFilterWhere( [
				'like',
				'orientation',
				$this->orientation
			] )
			->andFilterWhere( [
				'quantity' => $this->quantity
			] );

		return $dataProvider;
	}
}