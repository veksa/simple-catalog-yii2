<?
namespace app\modules\catalog\controllers;

use Yii;
use yii\web\Controller;
use yii\web\HttpException;
use app\modules\catalog\models\Category;
use app\modules\catalog\models\ItemSearch;

class DefaultController extends Controller{
	public function actionCategory( $id = NULL ){
		$model = new Category();

		$category = NULL;

		if( $id ){
			$category = $model->findOne( $id );
			if( $category === NULL ){
				throw new HttpException( 404, 'Not found' );
			}
		}

		$searchModel = new ItemSearch();
		$dataProvider = $searchModel->search( $id, Yii::$app->request->get() );

		echo $this->render( 'category', [
			'categoryProvider' => $model->getCategoryList( $id ),
			'itemProvider' => $dataProvider,
			'itemSearch' => $searchModel
		] );
	}

	public function actionDetail( $category, $id ){
		echo 'item';
    }
}
?>