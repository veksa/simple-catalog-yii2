<?
namespace app\modules\user\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface{
	public static function tableName(){
		return 'user';
	}

	public function getId(){
		return $this->getPrimaryKey();
	}

	public static function findByLogin( $login ){
		return static::findOne( [
			'login' => $login
		] );
	}

	public function validatePassword( $password ){
		return Yii::$app->security->validatePassword( $password, $this->password );
	}

	public function getAuthKey(){
		return $this->auth_key;
	}

	public function validateAuthKey( $authKey ){
		return $this->getAuthKey() === $authKey;
	}

	public static function findIdentityByAccessToken( $token, $type = NULL ){
		throw new NotSupportedException( '"findIdentityByAccessToken" is not implemented.' );
	}

	public static function findIdentity( $id ){
		return static::findOne( [
			'id' => $id
		] );
	}
}