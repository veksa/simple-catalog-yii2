<?
use yii\db\Schema;
use yii\db\Migration;

class m150703_131754_createCatalogModule extends Migration{
	public function up(){
		$tableOptions = NULL;
		if( $this->db->driverName === 'mysql' ){
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable( '{{%category}}', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . ' NOT NULL',
			'parent_id' => Schema::TYPE_INTEGER,
		], $tableOptions );

		$this->createTable( '{{%item}}', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . ' NOT NULL',
			'parent_id' => Schema::TYPE_INTEGER,
			'article' => Schema::TYPE_STRING,
			'brand_id' => Schema::TYPE_INTEGER,
			'model' => Schema::TYPE_STRING,
			'orientation' => Schema::TYPE_STRING,
			'quantity' => Schema::TYPE_INTEGER
		], $tableOptions );

		$this->createTable( '{{%brand}}', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . ' NOT NULL'
		], $tableOptions );

		$this->createTable( '{{%color}}', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . ' NOT NULL'
		], $tableOptions );

		$this->createTable( '{{%item_color}}', [
			'id' => Schema::TYPE_PK,
			'item_id' => Schema::TYPE_INTEGER,
			'color_id' => Schema::TYPE_INTEGER
		], $tableOptions );

		$this->createTable( '{{%size}}', [
			'id' => Schema::TYPE_PK,
			'name' => Schema::TYPE_STRING . ' NOT NULL'
		], $tableOptions );

		$this->createTable( '{{%item_size}}', [
			'id' => Schema::TYPE_PK,
			'item_id' => Schema::TYPE_INTEGER,
			'size_id' => Schema::TYPE_INTEGER
		], $tableOptions );

		$this->createIndex( 'IX_catalog_category', '{{%category}}', 'parent_id' );
		$this->addForeignKey( 'FK_catalog_category', '{{%category}}', 'parent_id', '{{%category}}', 'id', 'SET NULL', 'CASCADE' );

		$this->createIndex( 'IX_catalog_item', '{{%item}}', 'parent_id' );
		$this->addForeignKey( 'FK_catalog_item', '{{%item}}', 'parent_id', '{{%category}}', 'id', 'SET NULL', 'CASCADE' );

		$this->createIndex( 'IX_item_brand', '{{%item}}', 'brand_id' );
		$this->addForeignKey( 'FK_item_brand', '{{%item}}', 'brand_id', '{{%brand}}', 'id', 'SET NULL', 'CASCADE' );

		$this->createIndex( 'IX_item_color_item', '{{%item_color}}', 'item_id' );
		$this->addForeignKey( 'FK_item_color_item', '{{%item_color}}', 'item_id', '{{%item}}', 'id', 'CASCADE', 'CASCADE' );

		$this->createIndex( 'IX_item_color_color', '{{%item_color}}', 'color_id' );
		$this->addForeignKey( 'FK_item_color_color', '{{%item_color}}', 'color_id', '{{%color}}', 'id', 'CASCADE', 'CASCADE' );

		$this->createIndex( 'IX_item_size_item', '{{%item_size}}', 'item_id' );
		$this->addForeignKey( 'FK_item_size_item', '{{%item_size}}', 'item_id', '{{%item}}', 'id', 'CASCADE', 'CASCADE' );

		$this->createIndex( 'IX_item_size_size', '{{%item_size}}', 'size_id' );
		$this->addForeignKey( 'FK_item_size_size', '{{%item_size}}', 'size_id', '{{%size}}', 'id', 'CASCADE', 'CASCADE' );
	}

	public function down(){
		$this->dropTable( '{{%item_color}}' );
		$this->dropTable( '{{%item_size}}' );
		$this->dropTable( '{{%item}}' );
		$this->dropTable( '{{%color}}' );
		$this->dropTable( '{{%size}}' );

		$this->dropTable( '{{%category}}' );

		$this->dropTable( '{{%brand}}' );
	}
}
