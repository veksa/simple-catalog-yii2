<?
use yii\db\Schema;
use yii\db\Migration;

class m150703_130115_createUserTable extends Migration{
	public function up(){
		$tableOptions = NULL;
		if( $this->db->driverName === 'mysql' ){
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
		}

		$this->createTable( '{{%user}}', [
			'id' => Schema::TYPE_PK,
			'login' => Schema::TYPE_STRING . ' NOT NULL',
			'password' => Schema::TYPE_STRING . ' NOT NULL',
			'auth_key' => Schema::TYPE_STRING . '(32) NULL DEFAULT NULL'
		], $tableOptions );

		$this->createIndex( 'idx_user_login', '{{%user}}', 'login' );

		$this->insert( '{{%user}}', [
			'login' => 'admin',
			'password' => Yii::$app->security->generatePasswordHash( 'admin' )
		] );
	}

	public function down(){
		$this->dropTable( '{{%user}}' );
	}
}
