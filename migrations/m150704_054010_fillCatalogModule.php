<?
use yii\db\Migration;

use app\modules\catalog\models\Parser;

class m150704_054010_fillCatalogModule extends Migration{
	public function up(){
		$data = Parser::$catalog;
		if( $data && is_array( $data ) ){
			foreach( $data as $name => $subdata ){
				$this->insert( '{{%category}}', [
					'name' => $name,
					'parent_id' => NULL
				] );

				$id = $this->db->getLastInsertID();
				foreach( $subdata as $subname => $makros ){
					$this->insert( '{{%category}}', [
						'name' => $subname,
						'parent_id' => $id
					] );
				}
			}
		}

		$brands = Parser::$brands;
		if( $brands && is_array( $brands ) ){
			foreach( $brands as $name => $makros ){
				$this->insert( '{{%brand}}', [
					'name' => $name
				] );
			}
		}

		$colors = Parser::$colors;
		if( $colors && is_array( $colors ) ){
			foreach( $colors as $name => $makros ){
				$this->insert( '{{%color}}', [
					'name' => $name
				] );
			}
		}

		$sizes = array_merge( Parser::$clothes_sizes, Parser::$stick_sizes, Parser::$skates_sizes );
		if( $sizes && is_array( $sizes ) ){
			foreach( $sizes as $name => $makros ){
				$this->insert( '{{%size}}', [
					'name' => $name
				] );
			}
		}
	}

	public function down(){
		$this->dropForeignKey( 'FK_item_color_item', '{{%item_color}}' );
		$this->dropForeignKey( 'FK_item_color_color', '{{%item_color}}' );
		$this->dropForeignKey( 'FK_item_size_item', '{{%item_size}}' );
		$this->dropForeignKey( 'FK_item_size_size', '{{%item_size}}' );

		$this->truncateTable( '{{%item}}' );

		$this->truncateTable( '{{%item_color}}' );
		$this->truncateTable( '{{%item_size}}' );

		$this->truncateTable( '{{%color}}' );
		$this->truncateTable( '{{%size}}' );

		$this->addForeignKey( 'FK_item_color_item', '{{%item_color}}', 'item_id', '{{%item}}', 'id', 'SET NULL', 'CASCADE' );
		$this->addForeignKey( 'FK_item_color_color', '{{%item_color}}', 'color_id', '{{%color}}', 'id', 'CASCADE', 'CASCADE' );
		$this->addForeignKey( 'FK_item_size_item', '{{%item_size}}', 'item_id', '{{%item}}', 'id', 'SET NULL', 'CASCADE' );
		$this->addForeignKey( 'FK_item_size_size', '{{%item_size}}', 'size_id', '{{%size}}', 'id', 'CASCADE', 'CASCADE' );

		$this->dropForeignKey( 'FK_catalog_item', '{{%item}}' );

		$this->truncateTable( '{{%category}}' );

		$this->addForeignKey( 'FK_catalog_item', '{{%item}}', 'parent_id', '{{%category}}', 'id', 'SET NULL', 'CASCADE' );

		$this->dropForeignKey( 'FK_item_brand', '{{%item}}' );

		$this->truncateTable( '{{%brand}}' );

		$this->addForeignKey( 'FK_item_brand', '{{%item}}', 'brand_id', '{{%brand}}', 'id', 'SET NULL', 'CASCADE' );
	}
}
